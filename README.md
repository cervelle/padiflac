# PadiFlac

*PadiFlac* est une bibliotheque de sychronisation de page web *très* légère.

### Pour installer le front
    - Installer [opam](https://opam.ocaml.org), opam install OCaml, ( apt install opam, brew install opam, ...)
    - Installer les dependances : `opam install ocamlbuild ocamlfind js_of_ocaml js_of_ocaml-ppx cohttp-lwt-unix`
    - Dans le répertoire front, taper `make install`
    - Dans le répertoire front/samples, taper 'make nomdemonfichier.js' le fichier 'nomdemonfichier.ml' est compilé et transformé en javascript. Si la page html n'existe pas déjà, elle est créée.

La documentation est disponible ici https://cervelle.pages.lacl.fr/padiflac/doc/padiflac/Padiflac .

### Pour Utilisation

pour compiler vers javascript. Le fichier `fich.js` peut être chargé par la page web qui ne doit peut contenir que des en-têtes. Le code javascript ne pouvant pas être exécuté par le code ocaml, il est inutile de mettre des script dans la page. L'intégralité du code doit être écrit en ocaml.

    
