open Padiflac
open SimpleEvent
open Js_of_ocaml

let _ =
  Js.export "Padiflac"
    (object%js
       method connect channel =
         let send, _, handler = State.connect (Js.to_string channel) () in
         object%js
           method send value = send (Js.to_string value)

           method eventloop callback =
             State.event_loop (fun _ _ v -> callback (Js.string v)) handler;
             ()
         end
    end)
