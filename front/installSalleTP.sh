#!/bin/sh
opam init -y
opam update
eval $(opam config env)
opam install ocamlfind ocamlbuild dune js_of_ocaml-ppx -y
wget https://git.lacl.fr/cervelle/padiflac/-/archive/for-ocaml_4.5/padiflac-for-ocaml_4.5.zip
unzip padiflac-for-ocaml_4.5.zip
cd padiflac-for-ocaml_4.5/front
make
