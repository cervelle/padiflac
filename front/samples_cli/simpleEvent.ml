let nonceString =
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

let _ = Random.self_init ()

let gen_nonce () =
  let buff = Bytes.create 64 in
  for i = 0 to 63 do
    let c = Random.int 62 in
    Bytes.set buff i nonceString.[c]
  done;
  Bytes.to_string buff

let addr = "https://prog-reseau-m1.lacl.fr/padiflac/"

(* let print_str s =
     let n = String.length s in
     for i = 0 to n - 1 do
       Printf.printf "[%c:%i]" s.[i] (int_of_char s.[i])
     done;
   Printf.printf ":%i\n" n *)

let string_of_json e = Marshal.to_string e []

let sub s i l =
  try String.sub s i l
  with Invalid_argument _ as e ->
    Printf.printf "Error with sub %d %d %s\n" i l s;
    raise e

let ios s =
  try int_of_string s
  with Failure _ as e ->
    Printf.printf "Error with int_of_string %s\n" s;
    raise e

let rec parse_events s i =
  (* Printf.printf "parse_events: %d %s\n" i s;*)
  let n = String.length s in
  match String.index_from_opt s i '|' with
  | Some j ->
      let sizes = sub s i (j - i) in
      let size = ios sizes in
      let content = sub s (j + 1) size in
      let id, q = parse_events s (j + size + 1) in
      (id, content :: q)
  | None -> (ios (sub s i (n - i)), [])

let fold_with_last f init l =
  let ch_f last state value =
    try f last state value
    with e ->
      prerr_endline @@ "Handler raised exception: " ^ string_of_json e;
      state
  in
  let rec aux state = function
    | [] -> state
    | [ x ] -> ch_f true state x
    | x :: t -> aux (ch_f false state x) t
  in
  aux init l

let send canal data =
  let open Lwt in
  let open Cohttp_lwt_unix in
  Client.post
    ~body:(Cohttp_lwt.Body.of_string data)
    (Uri.of_string (addr ^ canal ^ "?nonce=" ^ gen_nonce ()))
  >>= fun (resp, _) ->
  let code = resp |> Response.status |> Cohttp.Code.code_of_status in
  if code <> 200 then
    prerr_endline ("Erreur de connection: " ^ string_of_int code);
  Lwt.return_unit

type callback = bool -> string -> unit
type handler = callback -> unit Lwt.t

let connect canal =
  let id = ref 0 in
  let send e = ignore @@ send canal e in

  let rec event_loop cb =
    let open Lwt in
    let open Cohttp_lwt_unix in
    let id2 = if !id = 0 then "" else "?id=" ^ string_of_int !id in
    let _ =
      Client.get (Uri.of_string (addr ^ canal ^ id2)) >>= fun (resp, body) ->
      let code = resp |> Response.status |> Cohttp.Code.code_of_status in
      if code <> 200 then (
        prerr_endline ("Erreur de connection: " ^ string_of_int code);
        Lwt.return ())
      else
        Cohttp_lwt.Body.to_string body >>= fun body_string ->
        try
          (*Printf.printf "parseenvent:%i:%s\n" (String.length v) v;*)
          (try
             let id2, vl = parse_events body_string 0 in
             id := id2;
             fold_with_last (fun b () s -> cb b s) () vl
           with
          | Exit -> raise Exit
          | _ -> prerr_endline "Bug parsing event ");
          event_loop cb
        with Exit -> Lwt.return ()
    in
    Lwt.return ()
  in
  ((send, event_loop) : (string -> unit) * handler)

let connect_std () =
  let open Lwt in
  let rec aux callback =
    Lwt_io.(read_line stdin) >>= fun l ->
    callback true l;
    aux callback
  in
  ((print_endline, aux) : (string -> unit) * handler)

let event_loop (callback : callback) (event_loop : handler) =
  Lwt_main.run (event_loop callback)
