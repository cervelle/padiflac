type callback = bool -> string -> unit
type handler = callback -> unit Lwt.t

val connect : string -> (string -> unit) * handler
val connect_std : unit -> (string -> unit) * handler
val event_loop : callback -> handler -> unit
