open SimpleEvent

let send, handle = connect_std ()
let send_net, handle_net = connect "cli_chat"
let rcv_from_net _ event = send event
let rcv_from_std _ event = send_net event

let _ =
  event_loop rcv_from_net handle_net;
  event_loop rcv_from_std handle
