type _private
(** private inner data used by this module *)

type t = {
  data : _private;
  append : t list -> unit;
  set : t list -> unit;
  setVisible : bool -> unit;
  show : unit -> unit;
  hide : unit -> unit;
  scroll_to_bottom : unit -> unit;
  value : unit -> string;
  set_value : string -> unit;
  on_action : (unit -> unit) -> unit
}
(** type of HTML nodes *)

val alert : string -> unit
(** Javascript alert *)

(** {2 HTML elements and text node} *)

val br : unit -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/br} <br> } *)

val text : string -> t
(** {{:https://developer.mozilla.org/fr/docs/Web/API/Document/createTextNode} text node } (not a tag) *)

val img : ?class_:string -> ?id:string -> ?alt:string -> string -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/img} <img> } *)

val canvas : ?class_:string -> ?id:string -> unit -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/canvas} <canvas> } *)

val a :
  ?class_:string ->
  ?id:string ->
  ?href:string ->
  t list ->
  t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/a} <a> } *)

val kbd : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/kbd} <kbd> } *)

val td : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/td} <td> } *)

val tr : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/tr} <tr> } *)

val li : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/li} &lt;li> } *)

val ul : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/ul} &lt;ul> } *)

val hi : int -> ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/h1} &lt;h1> } , h2 ... *)

val p : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/p} <p> } *)

val pre : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/pre} <pre> } *)

val div : ?class_:string -> ?id:string -> ?title:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/div} <div> } *)
val div' : ?class_:string -> ?id:string -> ?title:string -> string-> t


val table : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/table} <table> } *)

val span : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/span} <span> } *)

(** {2 Form elements}
 tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/input} <input> } *)

val button : ?class_:string -> ?id:string -> t list -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/button} <button> } *)

val checkbox_input : ?class_:string -> ?id:string -> bool -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input/checkbox} <input type="checkbox"> } *)

val radio_input :
  ?class_:string -> ?id:string -> string -> bool -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input/radio} <input type="radio"> } *)

val text_input : ?class_:string -> ?id:string -> string -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input/text} <input type="text"> } *)

val option : ?def:bool -> string -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/option} <option> } *)

val select :
  ?class_:string ->
  ?id:string ->
  ?def:string ->
  string list ->
  t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/select} <select> } *)

val string_of_date : float option -> string
(** converts a date to a string using a {{:https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date} Date } object *)

val date_of_string : string -> float option
(** converts a string to a date using {{:https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/parse} Date.parse() } *)

val date_input :
  ?class_:string -> ?id:string -> float option -> t
(** tag {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input/date} <input type="date"> } *)

val text_area :
  ?class_:string ->
  ?id:string ->
  ?is_read_only:bool ->
  string ->
  t
(** balise {{:https://developer.mozilla.org/fr/docs/Web/HTML/Element/textarea} <textarea> } *)

val run : t list -> unit
(** start web application giving the page content *)

val id_run : string -> t list -> unit
(** start web application giving the page content *)


(** {2 AJAX} *)

val get_http_request : string -> ((string, string) result -> unit) -> unit
(** GET HttpRequest *)

val post_http_request :
  string -> string -> ((string, string) result -> unit) -> unit
(** POST HttpRequest *)

val on_key_down : (string -> unit) -> unit
(** adds a handler called on key press. See {{:https://www.w3.org/TR/uievents-code} this page } for the possible strings passed to the handler *)

val storage_get : string -> string
(** fonction {{:https://developer.mozilla.org/fr/docs/Web/API/Storage/getItem} getItem() } *)

val storage_set : string -> string -> bool
(** fonction {{:https://developer.mozilla.org/fr/docs/Web/API/Storage/setItem} setItem() } *)

type tid

val set_timeout : (unit -> unit) -> float -> tid

val clear_timeout : tid -> unit
