open Runtime
open Html

let canal = "dsfsdfsdfsd"

type event = string

let lastid = ref ""

let display send =
  [
    text !lastid;
    br ();
    button ~on_click:(fun () -> send ">") [ text ">" ];
    button ~on_click:(fun () -> send "=") [ text "=" ];
    button ~on_click:(fun () -> send "<") [ text "<" ];
  ]

let handle_event evt _ =
  match evt with "<" | ">" | "=" -> () | _ -> lastid := evt
