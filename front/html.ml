open Js_of_ocaml

type _private = { node : Dom.node Js.t; as_text : unit -> Js.js_string Js.t }

type t = {
  data : _private;
  append : t list -> unit;
  set : t list -> unit;
  setVisible : bool -> unit;
  show : unit -> unit;
  hide : unit -> unit;
  scroll_to_bottom : unit -> unit;
  value : unit -> string;
  set_value : string -> unit;
  on_action : (unit -> unit) -> unit
}

let append_node parent node = ignore @@ parent##appendChild node.data.node
let append_nodes parent nodes = List.iter (append_node parent) nodes
let sc v class_ = Option.iter (fun x -> v##.className := Js.string x) class_
let si v id = Option.iter (fun x -> v##.id := Js.string x) id
let st v title = Option.iter (fun title -> v##.title := Js.string title) title
let scroll_to_bottom elem = elem##.scrollTop := elem##.scrollHeight

let make ?class_ ?id ?title ?(value = fun () -> "") ?(set_value = fun _ -> ()) ?(on_action = fun _ -> ()) x
    =
  let show () = x##.style##.display := Js.string "" in
  let hide () = x##.style##.display := Js.string "none" in
  sc x class_;
  si x id;
  st x title;
  {
    data = { node = (x :> Dom.node Js.t); as_text = (fun () -> x##.innerText) };
    append = (fun nodes -> append_nodes x nodes);
    set =
      (fun nodes ->
        x##.innerHTML := Js.string "";
        append_nodes x nodes);
    setVisible = (function true -> show () | false -> hide ());
    show;
    hide;
    scroll_to_bottom = (fun () -> scroll_to_bottom x);
    value;
    set_value;
    on_action
  }

let make_cont ?class_ ?id ?title ?value ?set_value ?on_action x items =
  append_nodes x items;
  make ?class_ ?id ?title ?value ?set_value ?on_action x

let alert x = Dom_html.window##alert (Js.string x)

let br () =
  let b = Dom_html.(createBr document) in
  let show () = b##.style##.display := Js.string "" in
  let hide () = b##.style##.display := Js.string "none" in
  {
    data = { node = (b :> Dom.node Js.t); as_text = (fun _ -> Js.string "") };
    append = (fun _ -> ());
    set = (fun _ -> ());
    setVisible = (function true -> show () | false -> hide ());
    show;
    hide;
    scroll_to_bottom = (fun () -> ());
    value = (fun () -> "");
    set_value = (fun _ -> ());
    on_action = (fun _ -> ())
  }

let text value =
  let saved = ref None in
  let text = Dom_html.document##createTextNode (Js.string value) in
  let show () = Option.iter (fun v -> text##.data := v ; saved := None) !saved in
  let hide () = if Option.is_none !saved then
      (saved := Some text##.data;
       text##.data := Js.string "")
  in
  let clear () = match !saved with
      None -> text##.data := Js.string ""
    | Some _ -> saved:= Some (Js.string "")
  in
  let append_one node = match !saved with
      None -> text##appendData (node.data.as_text ())
    | Some v -> saved := Some (v##concat (node.data.as_text()))
  in
  let append nodes = List.iter (fun s -> append_one s) nodes in
  {
    data = { node = (text :> Dom.node Js.t); as_text = (fun _ -> text##.data) };
    append;
    set =
      (fun nodes ->
         clear ();
         append nodes);
    setVisible = (function true -> show () | false -> hide ());
    show;
    hide;
    scroll_to_bottom = (fun () -> ());
    value = (fun () -> "");
    set_value = (fun _ -> ());
    on_action = (fun _ -> ())
  }

let img ?class_ ?id ?alt src =
  let img = Dom_html.(createImg document) in
  Option.iter (fun x -> img##.alt := Js.string x) alt;
  img##.src := Js.string src;
  make ?class_ ?id img
  

let canvas ?class_ ?id () =
  let ca = Dom_html.(createCanvas document) in
  make ?class_ ?id ca

let a ?class_ ?id ?href items =
  let a = Dom_html.(createA document) in
  Option.iter (fun href -> a##.href := Js.string href) href;
  let on_action on_click =
    let js_on_click _ =
      on_click ();
      Js._false
    in
    a##.onclick := Dom.handler js_on_click
  in
  make_cont ?class_ ?id ~on_action a items

let kbd ?class_ ?id items =
  let kbd = Dom_html.document##createElement (Js.string "kbd") in
  make_cont ?class_ ?id kbd items

let button ?class_ ?id items =
  let button = Dom_html.(createButton document) in
  sc button class_;
  si button id;
  let on_action on_click =
    let js_on_click _ =
      on_click ();
      Js._true
    in
    button##.onclick := Dom.handler js_on_click
  in
  make_cont ?class_ ?id ~on_action button items

let td ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createTd document) items

let tr ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createTr document) items

let li ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createLi document) items

let ul ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createUl document) items

let hi i ?class_ ?id items =
  let hib =
    Dom_html.document##createElement (Js.string ("H" ^ string_of_int i))
  in
  make_cont ?class_ ?id hib items

let p ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createP document) items

let pre ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createPre document) items

let div ?class_ ?id ?title items =
  make_cont ?class_ ?id ?title Dom_html.(createDiv document) items

let div' ?class_ ?id ?title s =
  let d = Dom_html.(createDiv document) in
  d##.innerHTML := Js.string s;
  make ?class_ ?id ?title d
  
let table ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createTable document) items

let span ?class_ ?id items =
  make_cont ?class_ ?id Dom_html.(createSpan document) items

let error_check s =
  print_endline @@ "checked or unchecled expected (received " ^ s ^ ")"

let make_checked_input ?class_ ?id input checked =
  input##.checked := Js.bool checked;
  let on_action on_change =
    let on_click _ =
      on_change ();
      Js._true
    in
    input##.onclick := Dom.handler on_click
  in
  let set_ctrl = function
    | "checked" -> input##.checked := Js._true
    | "unchecked" -> input##.checked := Js._false
    | s -> error_check s
  in
  let get_ctrl () =
    if Js.to_bool input##.checked then "checked" else "unchecked"
  in
  make ?class_ ?id ~value:get_ctrl ~set_value:set_ctrl ~on_action input

let checkbox_input ?class_ ?id checked =
  let input = Dom_html.(createInput ~_type:(Js.string "checkbox") document) in
  make_checked_input ?class_ ?id input checked

let radio_input ?class_ ?id name checked =
  let input =
    Dom_html.(
      createInput ~name:(Js.string name) ~_type:(Js.string "radio") document)
  in
  make_checked_input ?class_ ?id input checked

let make_text_input ?class_ ?id input value =
  input##.value := Js.string value;
  let on_action on_change =
    let on_input _ =
      on_change ();
      Js._true
    in
    input##.onchange := Dom.handler on_input
  in
  let set_value value = input##.value := Js.string value in
  let get_value () = Js.to_string input##.value in
  make ?class_ ?id ~value:get_value ~set_value ~on_action input

let text_input ?class_ ?id value =
  let input = Dom_html.(createInput ~_type:(Js.string "text") document) in
  make_text_input ?class_ ?id input value

let option ?(def = false) value =
  let opt = Dom_html.(createOption document) in
  opt##.value := Js.string value;
  append_node opt (text value);
  if def then opt##.defaultSelected := Js._true;
  make opt

let select ?class_ ?id ?def sl =
  let sel = Dom_html.(createSelect document) in
  List.iter
    (fun s ->
      append_node sel
        (option ~def:(match def with None -> false | Some x -> x = s) s))
    sl;
  let on_action on_change =
    let on_input _ =
      on_change ();
      Js._true
    in
    sel##.oninput := Dom.handler on_input
  in
  let set_value value = match int_of_string_opt value with
      None -> print_endline "select.set_value requires an int"
    | Some i -> sel##.selectedIndex := i
  in
  let value () = string_of_int (sel##.selectedIndex) in
  make ?class_ ?id ~value ~set_value ~on_action sel

let string_of_date = function
  | None -> ""
  | Some value ->
      let d = new%js Js.date_fromTimeValue value in
      Printf.sprintf "%i-%.2i-%.2i" d##getFullYear (d##getMonth + 1) d##getDate

let date_of_string value =
  let d = Js.date##parse (Js.string value) in
  if Float.is_nan d then None else Some d

let date_input ?class_ ?id value =
  let input = Dom_html.(createInput ~_type:(Js.string "date") document) in
  let ds = string_of_date value in
  make_text_input ?class_ ?id input ds

let text_area ?class_ ?id ?(is_read_only = false) value =
  let input = Dom_html.(createTextarea document) in
  if is_read_only then
    input##setAttribute (Js.string "readonly") (Js.string "true");
  make_text_input ?class_ ?id input value


let add_load_eventlistener listener =
  ignore (Dom_html.addEventListenerWithOptions Dom_html.window Dom_html.Event.load ~once:Js._true ~passive:Js._true (Dom.handler listener))

let run htmls =
  let on_load _ =
    let body =
      let elements =
        Dom_html.window##.document##getElementsByTagName (Js.string "body")
      in
      Js.Opt.get (elements##item 0) (fun () -> failwith "Can't find body")
    in
    List.iter (fun node -> ignore @@ body##appendChild node.data.node) htmls;
    Js._false
  in
  (* Dom_html.window##.onload := Dom.handler on_load *)
  add_load_eventlistener on_load

let id_run id htmls =
  let on_load _ =
    let node_opt =
      Dom_html.window##.document##getElementById (Js.string id)
    in
    Js.Opt.case node_opt
      (fun () -> print_endline ("can't find element of id "^id);Js._false)
      (fun node -> 
         List.iter (fun child -> ignore @@ node##appendChild child.data.node) htmls;
         Js._false)
  in
  (* Dom_html.window##.onload := Dom.handler on_load *)
  add_load_eventlistener on_load

let blob_of_string st =
  let a = new%js Typed_array.uint8Array (String.length st) in
  for i = 0 to String.length st - 1 do
    Typed_array.set a i (Char.code st.[i])
  done;
  File.blob_from_any [ `arrayBuffer a##.buffer ]

let get_http_request url f =
  let open XmlHttpRequest in
  let req = create () in
  let url = Js.string url in
  req##.onreadystatechange :=
    Js.wrap_callback (fun () ->
        if req##.readyState = DONE then
          if req##.status = 200 then
            (*let msg = File.CoerceTo.string req##.response in
              f (Some msg)*)
            Js.Opt.case
              (File.CoerceTo.arrayBuffer req##.response)
              (fun () -> f (Error "Fail to coerce to arrayBuffer"))
              (fun x -> f (Ok (Typed_array.String.of_arrayBuffer x)))
          else f (Error ("HTTP Error:" ^ string_of_int req##.status)));
  req##_open (Js.string "GET") url Js._true;
  req##.responseType := Js.string "arraybuffer";
  req##send Js.null

let post_http_request url data f =
  let open XmlHttpRequest in
  let req = create () in
  let url = Js.string url in
  req##.onreadystatechange :=
    Js.wrap_callback (fun () ->
        if req##.readyState = DONE then
          if req##.status = 200 then
            Js.Opt.case req##.responseText
              (fun () -> f (Error "Empty answer"))
              (fun x -> f (Ok (Js.to_string x)))
          else f (Error ("HTTP Error:" ^ string_of_int req##.status)));
  req##_open (Js.string "POST") url Js._true;
  req##setRequestHeader (Js.string "Content-Type")
    (Js.string "application/octet-stream");
  req##send_blob (blob_of_string data)

let on_key_down f =
  let kp e =
    Js.Optdef.iter e##.code (fun s -> f (Js.to_string s));
    Js._true
  in
  Dom_html.document##.onkeydown := Dom.handler kp

let storage_get s =
  Js.Optdef.case
    Dom_html.window##.localStorage
    (fun () -> "")
    (fun ls ->
      Js.Opt.case
        (ls##getItem (Js.string s))
        (fun () -> "")
        (fun s -> Js.to_string s))

let storage_set s v =
  Js.Optdef.case
    Dom_html.window##.localStorage
    (fun () -> false)
    (fun ls ->
      ls##setItem (Js.string s) (Js.string v);
      true)

type tid = Dom_html.timeout_id_safe

let set_timeout f delay = Dom_html.setTimeout f delay

let clear_timeout id = Dom_html.clearTimeout id
