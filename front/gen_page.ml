module type PAGE = sig
  type event

  val canal : string
  val handle_event : event -> (event -> unit) -> unit
  val display : (event -> unit) -> Html.t list
end

module Make (P : PAGE) = struct
  open SimpleEvent.Polymorphe
      
  let run () =
    let send,event_loop = connect P.canal in
    let c = Html.div (P.display send) in
    let handler last event =
      let () = P.handle_event event send in
      if last then c.set (P.display send)
    in
    event_run handler event_loop [c]
end

type ('state, 'event) control = {
  state : 'state;
  sender : 'event -> unit;
  state_control : ('state -> 'state) -> unit;
}

module type PAGE' = sig
  type event
  type state

  val init_state : state
  val canal : string
  val up_state : (state, event) control -> event -> state
  val display : (state, event) control -> Html.t list
end

module Make' (P : PAGE') = struct
  open SimpleEvent.State_polymorphe
      
  let run () =
    let sender,set_state,event_loop = connect P.canal P.init_state in 
    let c = Html.div [] in    
    let rec state_control f =
      set_state (
        fun s ->
          let s2 = f s in
          (* could we add a test if s2 <> s ? *)          
          c.set (P.display { state=s2; sender; state_control });
          s2)
    in
    let handler last state event =
      let next = P.up_state { state; sender; state_control } event in
      if last then c.set (P.display {state = next ; sender ; state_control });
      next
    in
    state_control (fun s -> s); (* to initialize c content *)
    event_run handler event_loop [c]
end
