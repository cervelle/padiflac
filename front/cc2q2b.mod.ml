open Runtime
open Html

let canal = "dsfsdfsdfsd"

type event = string

let lastevent = ref ""

let display send =
  [
    text_input
      ~on_change:(function
        | "<" | ">" | "=" -> true
        | s ->
            send s;
            true)
      "";
    br ();
    text !lastevent;
  ]

let handle_event evt _ =
  match evt with "<" | ">" | "=" -> lastevent := evt | _ -> ()
