open Padiflac.Html

(* Modele *)

let baton = ref 21
let message = ref "À vous de jouer"
let player_turn = ref true

(* Vues *)

let make_baton () =
  List.init !baton (fun _ -> img "baton2.png")

let baton_vue = div (make_baton ())
let baton_text_vue = div [ text "Il reste 21 bâtons" ]
    
let message_vue = div [ text !message ]

(* Controleur *)

(* ne pas utiliser la reference baton directement, ne passer que par le controleur *)

let dec_baton_ctrl x =
  if x < !baton then begin
    baton := !baton - x;
    baton_vue.set (make_baton ());
    baton_text_vue.set [ text (Printf.sprintf "Il reste %d bâtons" !baton) ]
  end

let set_message_ctrl s =
  message := s;
  message_vue.set [ text !message ]

(* Contrôles *)

let bouton1 = button [ text "1" ]
let bouton2 = button [ text "2" ]
let bouton3 = button [ text "3" ]

let boutons = div [ bouton1; bouton2; bouton3 ]

let repond_baton x =
  if not !player_turn then begin
    player_turn := not !player_turn;
    dec_baton_ctrl (4-x);
    if (!baton = 1) then begin
      set_message_ctrl (Printf.sprintf "L'intelligence artificielle prend %d bâtons ; vous avez perdu" (4-x));
    end  
    else begin
      set_message_ctrl (Printf.sprintf "L'intelligence artificielle prend %d bâtons" (4-x));
      boutons.show ()
    end
  end
  

let joue_baton x =
  if !player_turn then begin
    player_turn := not !player_turn;
    dec_baton_ctrl x;
    boutons.hide();
    set_message_ctrl "...";
    ignore @@ set_timeout (fun () -> repond_baton x) 1500.
  end
  
let instal_handler () =
  bouton1.on_action (fun () -> joue_baton 1);
  bouton2.on_action (fun () -> joue_baton 2);
  bouton3.on_action (fun () -> joue_baton 3)

let _ =
  instal_handler ()
    
(* Main *)

let _ = run [ hi 1 [ text "Jeu de Nim" ] ; baton_vue ; baton_text_vue ; message_vue ; boutons ]
