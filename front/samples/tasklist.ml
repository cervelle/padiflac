open Padiflac
open Html
module IDMap = Map.Make (Int)

type event = New of int * string | Tick of int | Untick of int | Remove of int

let canal =
  (*"canalTest"*)
  "TaskList"

let top n state =
  IDMap.to_seq state
  |> Seq.map (fun (_, (textval, _, _)) -> String.lowercase_ascii textval)
  |> List.of_seq |> List.sort String.compare
  |> List.fold_left
       (fun (last, count, acc) v ->
         if v = last then (last, count + 1, acc)
         else (v, 1, (last, count) :: acc))
       ("", 0, [])
  |> fun (_, _, x) ->
  x
  |> List.sort (fun (_, i) (_, j) -> j - i)
  |> List.fold_left
       (fun (i, acc) (c, count) ->
         if i > n || count <= 1 then (i, acc) else (i + 1, c :: acc))
       (0, [])
  |> snd |> List.rev

(* La balise <div> qui va contenir tout les messages, le conteneur et sa fonction de mise à jour sont récupérés *)
let container = div ~id:"output" []
let container_top = div ~id:"top_choice" []

(* On définit la fonction à appeler à chaque nouveau message : elle ajoute le message au container *)
let update_state state = function
  | New (id, value) -> IDMap.add id (value, true, false) state
  | Tick id ->
      IDMap.update id
        (function Some (va, vi, _) -> Some (va, vi, true) | x -> x)
        state
  | Untick id ->
      IDMap.update id
        (function Some (va, vi, _) -> Some (va, vi, false) | x -> x)
        state
  | Remove id ->
      IDMap.update id
        (function Some (va, _, ti) -> Some (va, false, ti) | x -> x)
        state

let display send state =
  let nl =
    IDMap.to_seq state
    |> Seq.filter (fun (_, (_, b, _)) -> b)
    |> Seq.map (fun (id, (textval, _, nb)) ->
           let idstr = "item" ^ string_of_int id in
           let checkbox = checkbox_input ~id:idstr nb in
           checkbox.on_action (fun () ->
               match checkbox.value () with
               | "checked" -> send (Tick id)
               | _ -> send (Untick id));
           li
             [
               text (textval ^ " ");
               checkbox
               (*label ~class_:"form-check-label" ~forid:idstr [text textval];*);
             ])
    |> List.of_seq
  in
  container.set [ ul nl ];

  let top_elem =
    state |> top 20
    |> List.map (fun s ->
           let b = button [ text s ] in
           b.on_action (fun () -> send (New (Random.bits (), s)));
           b)
  in

  container_top.set top_elem

(* Connexion au canal : on donne la fonction à appeler à chaque nouveau message et on récupere la fonction d'envoi *)
let send, get_state, handler =
  SimpleEvent.State_polymorphe.connect canal IDMap.empty

let callback last state event =
  let nstate = update_state state event in
  if last then display send nstate;
  nstate

(* On définit le champ de texte pour envoyer un message, à chaque modification du champs de texte on appelle la fonction send *)
let field = text_input ~id:"input" ""
let remove = button [ text "Enlever cochés" ]

let page =
  field.on_action (fun () ->
      let data = field.value () in
      field.set_value "";
      send (New (Random.bits (), data)));
  remove.on_action (fun () ->
      get_state (fun state ->
          IDMap.to_seq state
          |> Seq.filter (fun (_, (_, _, b)) -> b)
          |> Seq.iter (fun (id, _) -> send (Remove id));
          state));
  div
    [
      div ~class_:"container-fluid" [ hi 2 [ text "Liste de Courses" ] ];
      br ();
      div [ container; container_top ];
      div ~class_:"flexcontainer" [ field; remove ];
    ]

(* À la fin on programme la construction de la page web en placant tous les éléments. Cette fonction de construction est passée en argument à run *)
let _ =
  SimpleEvent.State_polymorphe.event_run callback handler
    [
      div ~class_:"main container-fluid jumbotron"
        [
          div ~class_:"container-fluid" [ hi 1 [ text "Course" ] ]; br (); page;
        ];
    ]
