open Padiflac
open Html

let canal =
  (*"canalTest"*)
  "shifumiJson"

type sign = Rock | Paper | Scissors

let less_shifumi s1 s2 =
  match (s1, s2) with
  | Rock, Paper | Paper, Scissors | Scissors, Rock -> true
  | _ -> false

type state = Empty | One of int * sign | Two of (int * sign) * (int * sign)
type event = Play of int * sign | Restart

let state = ref Empty

let _ = Random.self_init ()    
let id = Random.int 1073741820
    
let handle_event ev _ =
  state :=
    match (!state, ev) with
    | _, Restart -> Empty
    | Empty, Play (i, s) -> One (i, s)
    | One (i1, s1), Play (i2, s2) -> Two ((i1, s1), (i2, s2))
    | _ -> Empty

let play_button send id play lbl link =
  let btn = button [ text lbl; img link ] in
  btn.on_action (fun () -> send (Play (id,play)));
  btn

let buttons send = 
  div
    [
      play_button send id Rock "Rock"
        "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Rock-paper-scissors_%28rock%29.png/100px-Rock-paper-scissors_%28rock%29.png";
      play_button send id Paper "Paper"
            "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Rock-paper-scissors_%28paper%29.png/100px-Rock-paper-scissors_%28paper%29.png";
      play_button send id Scissors "Scissors"  "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Rock-paper-scissors_%28scissors%29.png/100px-Rock-paper-scissors_%28scissors%29.png"
    ]

let button_restart send =
  let btn = button [ text "Restart" ] in
  btn.on_action (fun () -> send Restart);
  btn

let display send =
  let r =
    match !state with
    | Empty -> buttons send
    | One (i, _) when i <> id -> buttons send
    | One _ -> text "Waiting for the other player"
    | Two ((_, s1), (i2, s2)) ->
        let msg =
          if s1 = s2 then text "Draw"
          else if less_shifumi s1 s2 = (i2 = id) then text "You Win !"
          else text "You Loose !"
        in
        div
          [ msg; button_restart send ]
  in
  [ hi 1 [ text "Shifumi:" ]; r ]
