open Padiflac
open Html
open Printf
open Char
open String

(* state *)


let choose_word () =
  let _ = Random.self_init () in
  let i = Random.int (List.length Dico.dico) in
  List.nth Dico.dico i

let word =  ref ""
let guessed =  ref ""
let fails = ref 0

(* UI *)

let guessed_ui = div [ ]

let spacify s = String.init (2*(String.length s)-1)
    (fun i -> if i mod 2 == 1 then ' ' else s.[i/2])

let guessed_controller f =
  guessed := f !guessed;
  guessed_ui.set [ text (spacify !guessed) ]

let fails_ui = div [ ]

let fails_controller f =
  fails := f !fails;
  fails_ui.set [ text (sprintf "Échecs : %d" !fails) ]

let letter_button x = button [ text (sprintf "%c" x) ]

let letter_inputs =
  let letters = List.init 26 (fun x -> chr ((code 'A') + x)) in
  (List.map (letter_button) letters)

let input_ui = div letter_inputs

let restart_button = button [ text "Recommencer" ]

let restart_ui = div [ text "Gagné !" ; restart_button ]

(* domain code *)

let init() =
  word := choose_word ();
  guessed_controller (fun _ -> String.make (String.length !word) '_');
  fails_controller (fun _ -> 0)
      
let handle_char c =
  match index_opt !word c with
    None -> (
      fails_controller (fun x -> x+1) 
    )
  | Some _ -> (
      guessed_controller 
        (fun previous -> String.mapi (fun i c' ->
             if !word.[i] = c then c else c') previous);
      if !word = !guessed then (
        input_ui.hide ();
        restart_ui.show ();
      )
    )

(* user input handler *)

let _ =
  init ()

let _ =
  restart_ui.hide ();
  restart_button.on_action (fun () ->
      init ();
      List.iter (fun b -> b.show ()) letter_inputs;
      restart_ui.hide ();
      input_ui.show ()
    )

let _ =
  List.iteri (fun i b -> b.on_action (fun () ->
      b.hide ();
      handle_char (chr ((code 'a') + i))
    )) letter_inputs

let _ =
  run [ guessed_ui; fails_ui; input_ui ; restart_ui]
