open Padiflac
open Html
open SimpleEvent

let canal =
  (*"canalTest"*)
  "ChatTest"

let send, handler = connect canal
let container = div ~class_:"chat-messages-text" []
let input_field = text_input ~class_:"chat-input-text" ""

let _ =
  input_field.on_action (fun () ->
      send (input_field.value ());
      input_field.set_value "")

let callback _ event = container.append [ text event; br () ]

let _ =
  event_run callback handler
    [
      hi 1 [ text ("Canal:" ^ canal) ];
      div ~class_:"chat-window"
        [
          div ~class_:"chat-messages" [ container ];
          div ~class_:"chat-input" [ input_field ];
        ];
    ]
