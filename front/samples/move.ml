open Padiflac
open Html
open SimpleEvent.State_polymorphe

let multiple = "█"

let rec times x = function
  | 0 -> ""
  | 1 -> x
  | w when w mod 2 = 0 ->
      let g = times x (w / 2) in
      g ^ g
  | w ->
      let g = times x (w / 2) in
      x ^ g ^ g

let c2s c = String.make 1 c

let line l y xmax =
  let g = List.filter (fun (_, y', _) -> y = y') l in
  let h = List.sort (fun (x, _, _) (x', _, _) -> x - x') g in
  let rec aux x c s = function
    | [] -> s ^ c ^ times "·" (xmax - x) ^ "┃\n"
    | (x', _, _) :: t when x = x' -> aux x multiple s t
    | (x', _, c') :: t ->
        let s' = s ^ c ^ times "·" (x' - x - 1) in
        aux x' (c2s c') s' t
  in
  aux (-1) "" "┃" h

let show l xmax ymax =
  let top_border = "┏" ^ times "━" (xmax + 1) ^ "┓\n" in
  let bot_border = "┗" ^ times "━" (xmax + 1) ^ "┛\n" in
  let lines = List.init (ymax + 1) (fun y -> line l y xmax) in
  List.fold_left ( ^ ) top_border lines ^ bot_border

let xmax = 10
let ymax = 10

let stored_name () =
  let x = storage_get "nickname" in
  if x <> "" then x.[0] else ' '

let arena l = show l xmax ymax

let content = pre ~id:"content" [ text (arena []) ]

let collect lst =
  let rec aux l x = function
    | [] -> (l, x)
    | (_, _, c) :: t when List.mem c l -> aux l x t
    | (_, _, c) :: t -> aux (c :: l) (x + 1) t
  in
  aux [] 0 lst

let search l =
  let rec aux c = if List.mem (Char.chr c) l then aux (c + 1) else Char.chr c in
  aux (Char.code 'a')

let rec oldest = function
  | [] -> failwith "argument must have size 26"
  | [ (_, _, c) ] -> c
  | _ :: t -> oldest t

let choose lst =
  let l, x = collect lst in
  if x < 26 then search l else oldest lst

let get_name name lst =
  if name <> ' ' then name
  else
    let c = choose lst in
    let _ = storage_set "nickname" (c2s c) in
    c

let update l (x, y, c) =
  let g = List.filter (fun (_, _, c') -> c <> c') l in
  (x, y, c) :: g

let callback last (name, lst) event =
  let lst' = update lst event in
  if last then content.set [ text (arena lst') ];
  (name, lst')

let send,cbs,event_loop = connect "upec_arena" (stored_name (), [])

let rec find_me name = function
  | [] -> None
  | (x, y, name') :: _ when name = name' -> Some (x, y)
  | _ :: t -> find_me name t

let sum_range v dv vmax =
  let v' = v + dv in
  if v' < 0 then 0 else if v' > vmax then vmax else v'

let move dx dy (name, lst) =
  let name' = get_name name lst in
  let pos = find_me name' lst in
  let x', y' =
    match pos with
    | None -> (xmax / 2, ymax / 2)
    | Some (x, y) -> (sum_range x dx xmax, sum_range y dy ymax)
  in
  send (x', y', name');
  (name', lst)

let make_button x y t =
  let b = button [ text t ] in
  b.on_action (fun () -> cbs (move x y));
  b

let btn_up = make_button 0 (-1) "↑"
let btn_down = make_button 0 1  "↓"
let btn_left = make_button (-1) 0 "←"
let btn_right = make_button 1 0 "→" 

let btns =
  table
    [
      tr [ td []; td [ btn_up ]; td [] ];
      tr [ td [ btn_left ]; td []; td [ btn_right ] ];
      tr [ td []; td [ btn_down ]; td [] ];
    ]

let _ =
  on_key_down (function
    | "ArrowLeft" -> cbs (move (-1) 0)
    | "ArrowRight" -> cbs (move 1 0)
    | "ArrowUp" -> cbs (move 0 (-1))
    | "ArrowDown" -> cbs (move 0 1)
    | _ -> ())

let _ =  
  event_run callback
    event_loop
    [ content; btns ]
