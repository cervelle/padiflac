open Padiflac
open Html

let settext_check name f =
  let input = text_input "" in
  input.on_action (fun () -> f (input.value ()));
  div [ text name; input ]

let gettext_check name f =
  let out = text "" in
  let btn = button [ text name ] in
  btn.on_action (fun () -> out.set [ text (f ()) ]);
  div [ btn; out ]

let bool_check name f =
  let btn_true = button [ text "true" ] in
  let btn_false = button [ text "false" ] in
  btn_true.on_action (fun () -> f true);
  btn_false.on_action (fun () -> f false);
  div [ text name; btn_true; btn_false ]

let unit_check name f =
  let btn = button [ text name ] in
  btn.on_action f;
  div [ btn ]

let check name ?view ?(text_to_node = fun s -> button [ text s ]) t =
  div
    [
      hi 2 [ text name ];
      (match view with None -> div [ t ] | Some x -> x);
      settext_check "append" (fun s -> t.append [ text_to_node s ]);
      settext_check "set" (fun s -> t.set [ text_to_node s ]);
      bool_check "setVisible" (fun b -> t.setVisible b);
      unit_check "show" (fun () -> t.show ());
      unit_check "hide" (fun () -> t.hide ());
      gettext_check "value" (fun () -> t.value ());
      settext_check "set_value" (fun s -> t.set_value s);
      settext_check "on_action" (fun s -> t.on_action (fun () -> alert s));
    ]

let _ =
  run
    [
      hi 2 [ text "test of test elements" ];
      settext_check "settext_check" (fun s -> alert s);
      gettext_check "gettext_check" (fun () -> "test text");
      unit_check "unit_check" (fun () -> alert "test text");
      bool_check "bool_check" (fun b ->
          if b then alert "true" else alert "false");
      check "text_input" (text_input "");
      check "text" (text "some text");
      check "br" (br ());
      check "img"
        (img
           "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/OCaml_Logo.svg/320px-OCaml_Logo.svg.png");
      check "a" (a ~href:"https://www.lacl.fr" [ text "lacl" ]);
      check "kbd" (kbd [ text "ici" ]);
      (let t = td [ text "this cell" ] in
       check "td" ~view:(table [ tr [ td [ text "<" ]; t; td [ text ">" ] ] ]) t);
      (let t = tr [ td [ text "this row" ] ] in
       check "tr"
         ~view:
           (table [ tr [ td [ text "above" ] ]; t; tr [ td [ text "below" ] ] ])
         t);
      check "table" (table [ tr [ td [ text "one cell" ] ] ]);
      (let t = li [ text "this line" ] in
       check "li" ~view:(ul [ li [ text "above" ]; t; li [ text "below" ] ]) t);
      check "ul" (ul [ li [ text "one" ]; li [ text "two" ] ]);
      check "hi" (hi 3 [ text "title 3" ]);
      check "p" (p [ text "paragraph" ]);
      check "pre" (pre [ text "p4r4gr4ph" ]);
      check "div" (div [ text "division" ]);
      check "span" (span [ text "span" ]);
      check "button" (button [ text "OK" ]);
      check "checkbox" (checkbox_input false);
      (let r = radio_input "a" true in
       check "radio"
         ~view:
           (div
              [
                text "one";
                radio_input "a" false;
                br ();
                text "-->";
                r;
                br ();
                text "three";
                radio_input "a" false;
              ])
         r);
      (let o = option "the option" in
       let s = select [] in
       s.append [ option "above"; o; option "below" ];
       check "option" ~view:s ~text_to_node:(fun s -> text s) o);
      check "select"
        ~text_to_node:(fun s -> option s)
        (select ~def:"two" [ "one"; "two"; "three" ]);
      check "date" (date_input None);
      check "text_area" (text_area "Hello\nPadiflac");
      check "read only text area"
        (text_area ~is_read_only:true "Hello\nPadiflac");
    ]
