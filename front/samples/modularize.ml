let rec copy f g =
  try 
  let l =input_line f in
    output_string g (l^"\n");
    copy f g  
  with  End_of_file -> ()

let _ = 
  let infile = Sys.argv.(1) in
  let mod_name =  String.capitalize_ascii @@ Filename.chop_extension @@ Filename.basename infile in
  let f = if Array.length Sys.argv > 2 then open_out Sys.argv.(2) else stdout in
  Printf.fprintf f "open Padiflac\nmodule %s = struct\n" mod_name;
  
  let g = open_in infile in
  copy g f;
  close_in g; 
  
  Printf.fprintf f "end
  module R = Gen_page.Make (%s)
  let _= R.run ()\n" mod_name;
  close_out f;

  let pagename = (Filename.chop_extension infile)^".html" in
  
  if not @@ Sys.file_exists pagename then 
  let fhtml = open_out pagename in
  Printf.fprintf fhtml "<!DOCTYPE html><html><head>
  <title>Padiflac</title>
  <meta charset='utf-8'/>
  <script type='text/javascript' src='%s.bc.js'></script>
  </head><body></body></html>" mod_name;
  close_out fhtml

