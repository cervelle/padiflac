open Padiflac
open Html
open SimpleEvent.State_polymorphe

let canal =
  (*"canalTest"*)
  "ChatTest"

let send,_,event_loop = connect canal []
    
(* La balise <div> qui va contenir tout les messages, le conteneur et sa fonction de mise à jour sont récupérés *)

let container = div ~id:"output" []

let field =
  text_input ~id:"input"
    ""

let _ =
  field.on_action (fun () ->
      send (field.value ()) ;
      field.set_value "")

(* On définit la fonction à appeler à chaque nouveau message : elle ajoute le message au container *)
let handle_event last state event =
  if last then (
    container.append @@ List.rev @@ text event :: br () :: state;
    container.scroll_to_bottom ();
    [])
  else
    text event :: br() ::state

let _ = event_run
  handle_event
  event_loop
    [ div ~id:"title" [ text ("Canal:" ^ canal) ]; container; field ]
