open Padiflac
open Html
open Printf
open Char
open String

(* to remove once ocaml 4.13 is sufficiently deployed *)

let fold_left f init s =
  let rec aux i value =
    if i = String.length s then value else
      aux (i+1) (f value s.[i])
  in aux 0 init

(* state *)

module IntPair = struct
  type t = int *  int
  let compare (x,y) (x',y') =
    match Int.compare x x' with
      0 -> Int.compare y y'
    | c -> c
end

module P = Map.Make(IntPair)

type direction = H | V

let _ = Random.self_init ()

let dico5 = List.filter (fun s -> (length s) = 5) Smdico.dico
    
let dico6 = List.filter (fun s -> (length s) = 6) Smdico.dico

let choose_word () =
  let dic =
    if (Random.int 3) = 0 then dico6 else dico5 in
  let i = Random.int (List.length dic) in
  List.nth dic i

let shuffle letters =
  let with_int = List.map (fun c -> (Random.bits (), c)) letters in
  let sorted = List.sort (fun (x,_) (y,_) -> Int.compare x y) with_int in
  List.map snd sorted

let list_of_string w =
  List.init (String.length w) (fun i -> w.[i])

let rec test_merge src = function
    [] -> true
  | x::t' as cand ->
    match src with
      [] -> false
    | y::t when x=y -> test_merge t t'
    | _::t -> test_merge t cand

let same_letter w l =
  let l' = List.sort Char.compare (list_of_string w) in
  test_merge l l'
  
let compatible w =
  let l = List.sort Char.compare (list_of_string w) in
  List.filter (fun s -> s<>w && same_letter s l) Smdico.dico
    
(*
    
let good_words =
  List.filter (fun s -> 3 <= (List.length (compatible s))) dico5

let _ =
  List.iter print_endline good_words

*)

let print_grid (grid,l) =
  let minx,miny,maxx,maxy =
    P.fold (fun (x,y) _ (minx,miny,maxx,maxy) ->
        (min x minx, min y miny, max x maxx, max y maxy)
      ) grid (max_int,max_int,min_int,min_int)
  in
  print_endline("-----------");
  for y = miny to maxy do
    for x = minx to maxx do
      match P.find_opt (x,y) grid with
        None -> print_string "  "
      | Some c -> printf " %c" c
    done ;
    print_newline ()
  done;
  List.iter (fun ((x,y),d,s) -> printf "(%d,%d) %s : %s\n" x y (match d with H -> "H" | V -> "V") s) l;
  print_endline("-----------")


exception OK of (char P.t * ((int*int)*direction*string) list) * string

let opp = function H -> V | V -> H

let move (x,y) n = function
    H -> (x+n,y)
  | V -> (x,y+n)

let get_opt s x =
  if x<0 || x>= (String.length s) then None else Some (s.[x])

let letter x y ((sx,sy),dir,w) =
  match dir with
    H -> if y <> sy then None else
      get_opt w (x-sx)
  | V -> if x <> sx then None else
      get_opt w (y-sy)


exception Halt

let for_all_int x y pred =
  try
    for i=x to y do
      if not (pred i) then raise Halt
    done ; true
  with
    Halt -> false

let test c pos grid =
  match P.find_opt pos grid with
    None -> true
  | Some c' -> c=c'

let can_place w index pos dir grid =
  let l = String.length w in
  (* no letters before and after the word *)
  not (P.mem (move pos (l-index) dir) grid) &&
  not (P.mem (move pos (-1-index) dir) grid) &&
  (* all letters with word w are compatible with the grid *)
  for_all_int 0 (l-1) (fun i ->
      test w.[i] (move pos (i-index) dir) grid) &&
  (* word does not overlap another one : no consecutive occupied spaces *)
  for_all_int 0 (l-2) (fun i ->
      (not (P.mem (move pos (i-index) dir) grid)) ||
      (not (P.mem (move pos (i+1-index) dir) grid))) &&
  (* word does not touch other letters *)
  for_all_int 0 (l-1) (fun i ->
      let p = (move pos (i-index) dir) in
      if not (P.mem p grid) then
        let o = opp dir in
        (not (P.mem (move p (-1) o) grid)) &&
        (not (P.mem (move p 1 o) grid))
      else
        true)

let add_word w pos dir (grid,l) =
  let (out,_) =
  fold_left (fun (gr,i) c ->
        ((P.add (move pos i dir) c gr),i+1)) (grid,0) w
  in (out,(pos,dir,w)::l)

let add_if_can_place w index pos dir (grid,l) =
  if can_place w index pos dir grid then raise (OK ((add_word w (move pos (-index) dir) dir (grid,l)),w))

let try_add_word w (grid,l) =
  P.iter (fun pos c ->
      for i=0 to (String.length w)-1 do
        if c = w.[i] then (
          add_if_can_place w i pos H (grid,l);
          add_if_can_place w i pos V (grid,l)
        )
      done) grid

let try_add grid wlist =
  try
    List.iter (fun w -> try_add_word w grid) wlist;
    None
  with
    OK (grid,w) -> Some (grid, List.filter (fun w' -> w<>w') wlist) 

let rec repeat_until_none f v =
  match f v with
    None -> v
  | Some v' -> repeat_until_none f v'

let rec pre_build_grid () =
  let w = choose_word () in
  let comp = compatible w in
  if comp = [] then pre_build_grid () else
    let grid = P.empty in
    let first = add_word w (0,0) H (grid,[]) in
    let (grid,_) = repeat_until_none (fun (g,l) -> try_add g l) (first,comp) in
    let letters = list_of_string w in
    (letters,grid)

let build_grid () =
  let letters,(grid,words) = pre_build_grid () in
  let minx,miny,maxx,maxy =
    P.fold (fun (x,y) _ (minx,miny,maxx,maxy) ->
        (min x minx, min y miny, max x maxx, max y maxy)
      ) grid (max_int,max_int,min_int,min_int)
  in
  let words' = List.map (fun ((x,y),dir,w) -> ((x-minx,y-miny),dir,w)) words in
  (letters,words',maxx-minx,maxy-miny)

(* State *)

let words = ref []
let letters = ref []
let grid = ref [] (* array of char, either a letter of space for a letter to find and * for outside the grid *)
let input = ref ""

(* UI *)

let table_ui = table []

let c2s = String.make 1

let controller_grid f =
  grid := f !grid ;
  table_ui.set (
    List.map (fun l -> tr (
        List.map (function
              ' ' -> td ~class_:"borders" [text " "]
            | '*' -> td []
            | c -> td ~class_:"borders" [text (c2s c)]
          ) l)
      ) !grid
  )

let input_ui = text ""

let add_to_array w =
  controller_grid (fun array ->
      List.mapi (fun y l ->
          List.mapi (fun x c ->
              match letter x y w with
                None -> c
              | Some d -> d
            ) l
        ) array
    )

let restart_button = button [ text "Recommencer" ]
let restart_ui = div ~id:"restart" [ text "Gagné !" ; restart_button ]
let letters_ui = div ~id:"letters" []
let submit_button = button [ text "⏎" ]
let shuffle_button = button [ text "⟳" ]
let button_line_ui = div ~id:"button" [letters_ui ; div [submit_button ; shuffle_button]]


let gagne () =
  restart_ui.show ();
  button_line_ui.hide ()

let process_word w =
  List.iter (
    fun (pos,dir,w') ->
      if w=w' then (
        add_to_array (pos,dir,w);
        words := List.filter (fun (_,_,w') -> w<>w') !words;
        if !words = [] then
          gagne ()
      )
  ) !words

let controller_append_input c =
  input_ui.append [ text (c2s c) ];
  input := sprintf "%s%c" !input c

let controller_clear_input () =
  input_ui.set [ text "" ];
  input := ""

let buttons = ref []

let make_button c =
  let b = button [ text (c2s c) ] in
  b.on_action (fun () -> b.hide () ; controller_append_input c);
  b

let controller_letters f =
  letters := f !letters;
  buttons := (List.map make_button !letters);
  letters_ui.set (!buttons)

(*
let words = ref []
let letters = ref []
let grid = ref [[]] (* array of char, either a letter of space for a letter to find and * for outside the grid *)
let input = ref ""
*)

let make_array words maxx maxy =
  List.init (maxy+1)
    (fun y -> List.init (maxx+1)
        (fun x -> List.fold_left (
             fun c word -> match letter x y word with
                 None -> c
               | Some d -> ' '
           ) '*' words
        )
    )

let init () =
  let ltrs,wds,x,y = build_grid () in
  words := wds;
  controller_letters (fun _ -> shuffle ltrs);
  controller_grid (fun _ -> make_array wds x y);
  controller_clear_input ();
  restart_ui.hide ();
  button_line_ui.show ()

let _ =
  restart_button.on_action (fun () ->
      init ());
  shuffle_button.on_action (fun () ->
      controller_letters (fun l -> shuffle l);
      controller_clear_input ());
  submit_button.on_action (fun () ->
        process_word !input;
        controller_clear_input ();
        List.iter (fun b -> b.show ()) !buttons
      )


let _ =
  init ()

let _ =
  run [ table_ui ; div ~id:"input" [input_ui] ; button_line_ui; restart_ui ]

(* let (grid,l) = build_grid ()

   let _ =
   print_grid (grid,l) *)

(*
let choose_word () =
  let _ = Random.self_init () in
  let i = Random.int (List.length Dico.dico) in
  List.nth Dico.dico i

let word =  ref ""
let guessed =  ref ""
let fails = ref 0

(* UI *)

let guessed_ui = div [ ]

let spacify s = String.init (2*(String.length s)-1)
    (fun i -> if i mod 2 == 1 then ' ' else s.[i/2])

let guessed_controller f =
  guessed := f !guessed;
  guessed_ui.set [ text (spacify !guessed) ]

let fails_ui = div [ ]

let fails_controller f =
  fails := f !fails;
  fails_ui.set [ text (sprintf "Échecs : %d" !fails) ]

let letter_button x = button [ text (sprintf "%c" x) ]

let letter_inputs =
  let letters = List.init 26 (fun x -> chr ((code 'A') + x)) in
  (List.map (letter_button) letters)

let input_ui = div letter_inputs

let restart_button = button [ text "Recommencer" ]

let restart_ui = div [ text "Gagné !" ; restart_button ]

(* domain code *)

let init() =
  word := choose_word ();
  guessed_controller (fun _ -> String.make (String.length !word) '_');
  fails_controller (fun _ -> 0)
      
let handle_char c =
  match index_opt !word c with
    None -> (
      fails_controller (fun x -> x+1) 
    )
  | Some _ -> (
      guessed_controller 
        (fun previous -> String.mapi (fun i c' ->
             if !word.[i] = c then c else c') previous);
      if !word = !guessed then (
        input_ui.hide ();
        restart_ui.show ();
      )
    )

(* user input handler *)

let _ =
  init ()

let _ =
  restart_ui.hide ();
  restart_button.on_action (fun () ->
      init ();
      List.iter (fun b -> b.show ()) letter_inputs;
      restart_ui.hide ();
      input_ui.show ()
    )

let _ =
  List.iteri (fun i b -> b.on_action (fun () ->
      b.hide ();
      handle_char (chr ((code 'a') + i))
    )) letter_inputs

let _ =
  run [ guessed_ui; fails_ui; input_ui ; restart_ui] *)
