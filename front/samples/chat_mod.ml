open Padiflac
open Html

type event = string

let canal =
  (*"canalTest"*)
  "ChatTest"

(* La balise <div> qui va contenir tout les messages, le conteneur et sa fonction de mise à jour sont récupérés *)
let container = div ~id:"output" []

(* On définit la fonction à appeler à chaque nouveau message : elle ajoute le message au container *)

let handle_event event _ =
  container.append [ text event; br () ];
  container.scroll_to_bottom ()

let display send =
  (* On définit le champ de texte pour envoyer un message, à chaque modification du champs de texte on appelle la fonction send *)
  let field =
    text_input ~id:"input"
      ""
  in
  let handler () =
    send (field.value ()) ;
    field.set_value ""
  in
  field.on_action handler; 
  [ div ~id:"title" [ text ("Canal:" ^ canal) ]; container; field ]
(* À la fin on programme la construction de la page web en placant tous les éléments. Cette fonction de construction est passée en argument à run *)
