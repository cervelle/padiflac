open Padiflac
open Html

let canal =
  (*"canalTest"*)
  ref "ChatTest"

let send_fun = ref (fun _ -> ())

let newconnection ncanal =
  let container = div ~class_:"chat-messages-text" [] in

  let send, _, eloop = SimpleEvent.State.connect ncanal () in

  let callback _ _ event =
    if !canal <> ncanal then raise Exit;
    container.append [ text event; br () ]
  in

  send_fun := send;
  SimpleEvent.State.event_loop callback eloop;

  container

let allchatcontainer = div ~class_:"chat-messages" []
let canal_selector = text_input !canal

let _ =
  canal_selector.on_action (fun () ->
      let newcanal = canal_selector.value () in
      canal := newcanal;
      let container = newconnection newcanal in
      allchatcontainer.set [ container ])

let _ = allchatcontainer.set [ newconnection !canal ]
let input_field = text_input ~class_:"chat-input-text" ""

let _ =
  input_field.on_action (fun () ->
      !send_fun (input_field.value ());
      input_field.set_value "")

let _ =
  run
    [
      div
        [
          text "choose canal:";
          canal_selector;
          div ~class_:"chat-window"
            [ allchatcontainer; div ~class_:"chat-input" [ input_field ] ];
        ];
    ]
