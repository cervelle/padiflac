open Padiflac
open Html
open SimpleEvent.State_polymorphe

let canal = "canalTicTacToePoly"

type st = Empty | Cross | Circle

type event = bool * int * int option
(** Type of the canal: None -> restart game, b=true -> X play else O play, i j are coordinate*)

let initstate =
  ((Empty, Empty, Empty), (Empty, Empty, Empty), (Empty, Empty, Empty))

let map3 f (a, b, c) = (f a, f b, f c)
let get3 (a, b, c) = function 0 -> a | 1 -> b | _ -> c
let get33 st i j = get3 (get3 st j) i

let set3 (a, b, c) v = function
  | 0 -> (v, b, c)
  | 1 -> (a, v, c)
  | _ -> (a, b, v)

let set33 st v i j = set3 st (set3 (get3 st j) v i) j

let allfill (d, e, f) =
  let af (a, b, c) = a <> Empty && b <> Empty && c <> Empty in
  af d && af e && af f

let finish st = 
  let comb x y =
    match (x, y) with
    | Circle, _ | _, Circle -> Circle
    | Cross, _ | _, Cross -> Cross
    | _ -> Empty
  in
  let maj (a, b, c) = if a = b && a = c then a else Empty in
  let rowi i = maj (get3 st i) in
  let coli i = maj (map3 (fun x -> get3 x i) st) in
  let center = get33 st 1 1 in
  match
    comb
      (comb
         (comb (rowi 0) (comb (rowi 1) (rowi 2)))
         (comb (coli 0) (comb (coli 1) (coli 2))))
      (comb
         (maj (get33 st 0 0, center, get33 st 2 2))
         (maj (get33 st 0 2, center, get33 st 2 0)))
  with
  | Empty -> if allfill st then Some Empty else None
  | x -> Some x

let upstate st = function
  | None -> initstate
  | Some (b, i, j) ->
      let e = if b then Cross else Circle in
      set33 st e i j

let send, _, handler = connect canal (true, initstate)

let send_button b i j =
  let btn = button [ text (if b then "x" else "o") ] in
  btn.on_action (fun () -> send (Some (b, i, j)));
  btn

let display finish (b, state) =
  let but i j =
    match get33 state i j with
    | Empty when not finish -> td [ send_button b i j ]
    | Empty -> td [ text "_" ]
    | Circle -> td [ text "o" ]
    | Cross -> td [ text "x" ]
  in
  table
    [
      tr [ but 0 0; but 1 0; but 2 0 ];
      tr [ but 0 1; but 1 1; but 2 1 ];
      tr [ but 0 2; but 1 2; but 2 2 ];
    ]

let container = div [ display false (false, initstate) ]

let restart_button =
  let btn = button [ text "restart" ] in
  btn.on_action (fun () -> send None);
  btn

let receive _ (b, state) event =
  let state2 = upstate state event in
  match finish state2 with
  | Some p ->
      let msg =
        match p with
        | Empty -> "draw"
        | Cross -> "X win !"
        | Circle -> "O win !"
      in
      container.set
        [ text "Finish !"; text msg; display true (b, state2); restart_button ];
      (not b, initstate)
  | None ->
      container.set [ display false (b, state2) ];
      (not b, state2)

let _ =
  event_run receive handler [
  div ~id:"main" [ container ];
  div ~id:"footer" [ text ("canal: " ^ canal) ]]
