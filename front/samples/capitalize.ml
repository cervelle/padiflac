let _ = 
  let mod_name =  String.capitalize_ascii @@ Filename.chop_extension @@ Filename.basename Sys.argv.(1) in
  let f = if Array.length Sys.argv > 2 then open_out Sys.argv.(2) else stdout in
  Printf.fprintf f "open Padiflac
  module R = Gen_page.Make (%s)
  let _= R.run ()\n" mod_name;
  close_out f

