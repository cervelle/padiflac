(** {1 Padiflac event ocaml front API} *)

(** This module allows event broadcast and to setup a callback function called with new events.

    The connect function connects to a channel and provides
    - A function to send events
    - For the state and state_polymorphe versions, a function to change and consult the internal state.
    - A function to start the event loop

    The event_run function loads the web app in the browser and start the event loop. It is to be called as:
    {[event_run callback_function event_loop [ list of html elements ] ]}

    The callback function takes two or three elements:
    - The first one is a boolean and can be ignored by beginners. It indicates if the event received is the last one from the events sent by the server. If the value is false, it means that the callback will be called again immediatly and the the view need not to be refreshed. 
    - For the state and state_polymorphe version, the state is passed as the second parameter and the new state is to be returned.
    - The last element is the event iteself
    
    There are four implementations: plain, polymorphe, state and state_polymorphe.
    - For the simple version and the Polymorphe version, the state has to be taken care of by the developer using references
    - For the state version and the state_polymorphe version, the state is managed by the library: The handler is given the current state and returns the new state ;  The connect version provide the developer with a function which can consult and change the value of the state
    - For the simple and state versions, the event are strings
    - For the polymorphe and the state_polymorphe versions, the event can be any ocaml finite data type, sent as JSON *)

(** {b Simple version: events are string} *)

type callback = bool -> string -> unit
type handler

val connect : string -> (string -> unit) * handler
(*val connect_std : unit -> (string -> unit) * handler*)
val event_run : callback -> handler -> Html.t list -> unit
val event_loop : callback -> handler -> unit

(** {b Polymorphe version: events are any finite data ocaml type} *)

module Polymorphe : sig
  type 'event callback = bool -> 'event -> unit
  type 'event handler

  val connect : string -> ('event -> unit) * 'event handler
  val event_run : 'event callback -> 'event handler -> Html.t list -> unit
end

(** {b State version (for pure functional programming adepts)} *)

module State : sig
  type 'state callback = bool -> 'state -> string -> 'state
  type 'state handler

  val connect :
    string ->
    'state ->
    (string -> unit) * (('state -> 'state) -> unit) * 'state handler

  val event_loop : 'state callback -> 'state handler -> unit
  val event_run : 'state callback -> 'state handler -> Html.t list -> unit
end

(** {b State_polymorphe version (for pure functional programming adepts)} *)

module State_polymorphe : sig
  type ('state, 'event) callback = bool -> 'state -> 'event -> 'state
  type ('state, 'event) handler

  val connect :
    string ->
    'state ->
    ('event -> unit) * (('state -> 'state) -> unit) * ('state, 'event) handler

  val event_loop : ('state, 'event) callback -> ('state, 'event) handler -> unit

  val event_run :
    ('state, 'event) callback -> ('state, 'event) handler -> Html.t list -> unit
end
