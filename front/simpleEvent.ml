open Html

let nonceString =
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

let _ = Random.self_init ()

let gen_nonce () =
  let buff = Bytes.create 64 in
  for i = 0 to 63 do
    let c = Random.int 62 in
    Bytes.set buff i nonceString.[c]
  done;
  Bytes.to_string buff

let addr = "https://prog-reseau-m1.lacl.fr/padiflac/"

(* let print_str s =
     let n = String.length s in
     for i = 0 to n - 1 do
       Printf.printf "[%c:%i]" s.[i] (int_of_char s.[i])
     done;
   Printf.printf ":%i\n" n *)

let string_of_json e = Js_of_ocaml.(Js.to_string @@ Json.output e)

let json_of_string event =
  try Some Js_of_ocaml.(Json.unsafe_input (Js.string event))
  with _ ->
    prerr_endline "Fail to unserialized";
    None

let sub s i l =
  try String.sub s i l
  with Invalid_argument _ as e ->
    Printf.printf "Error with sub %d %d %s\n" i l s;
    raise e

let ios s =
  try int_of_string s
  with Failure _ as e ->
    Printf.printf "Error with int_of_string %s\n" s;
    raise e

let rec parse_events s i =
  (* Printf.printf "parse_events: %d %s\n" i s;*)
  let n = String.length s in
  match String.index_from_opt s i '|' with
  | Some j ->
      let sizes = sub s i (j - i) in
      let size = ios sizes in
      let content = sub s (j + 1) size in
      let id, q = parse_events s (j + size + 1) in
      (id, content :: q)
  | None -> (ios (sub s i (n - i)), [])

let fold_with_last f init l =
  let ch_f last state value =
    try f last state value
    with e ->
      prerr_endline @@ "Handler raised exception: " ^ string_of_json e;
      state
  in
  let rec aux state = function
    | [] -> state
    | [ x ] -> ch_f true state x
    | x :: t -> aux (ch_f false state x) t
  in
  aux init l

let send canal data =
  Html.post_http_request
    (addr ^ canal ^ "?nonce=" ^ gen_nonce ())
    data
    (function
      | Error err -> prerr_endline ("Erreur de connection:" ^ err) | Ok _ -> ())

module State = struct
  type 'state callback = bool -> 'state -> string -> 'state
  type 'state handler = 'state callback -> unit

  let connect canal (state : 'state) =
    let stref = ref state in
    let id = ref 0 in
    let send = send canal in

    (* TODO option not to see its own event using id *)
    let call_state f = stref := f !stref in

    let rec event_loop cb =
      let id2 = if !id = 0 then "" else "?id=" ^ string_of_int !id in
      Html.get_http_request
        (addr ^ canal ^ id2)
        (function
          | Error err -> prerr_endline ("Erreur de connection:" ^ err)
          | Ok v -> (
              try
                (*Printf.printf "parseenvent:%i:%s\n" (String.length v) v;*)
                (try
                   let id2, vl = parse_events v 0 in
                   id := id2;
                   stref := fold_with_last cb !stref vl
                 with
                | Exit -> raise Exit
                | _ -> prerr_endline "Bug parsing event ");
                event_loop cb
              with Exit -> ()))
    in
    (send, call_state, (event_loop : 'state handler))

  let event_loop callback event_loop = event_loop callback

  let event_run callback event_loop page =
    event_loop callback;
    run page
end

type callback = bool -> string -> unit
type handler = callback -> unit

let connect canal =
  let send, _, event_loop = State.connect canal () in
  (send, fun cb -> event_loop (fun b _ e -> cb b e))

(*let connect_std () =
  let rec aux callback =
    let l = read_line () in
    callback true l;
    aux callback
  in
  (print_endline, aux)*)

let event_run callback event_loop page =
  event_loop callback;
  run page

let event_loop callback event_loop = event_loop callback

module State_polymorphe = struct
  type ('state, 'event) callback = bool -> 'state -> 'event -> 'state
  type ('state, 'event) handler = ('state, 'event) callback -> unit

  let connect canal state =
    let msend send e = send @@ string_of_json e in
    let send, cs, event_loop = State.connect canal state in
    let el cb =
      event_loop (fun b s e ->
          let obj_opt = json_of_string e in
          Option.fold ~none:s ~some:(fun obj -> cb b s obj) obj_opt)
    in
    (msend send, cs, el)

  let event_loop callback event_loop = event_loop callback

  let event_run callback event_loop page =
    event_loop callback;
    run page
end

module Polymorphe = struct
  type 'event callback = bool -> 'event -> unit
  type 'event handler = 'event callback -> unit

  let connect canal =
    let send, _, el = State_polymorphe.connect canal () in
    (send, fun (cb : 'event callback) -> el (fun b _ e -> cb b e))

  let event_run callback event_loop page =
    event_loop callback;
    run page
end
