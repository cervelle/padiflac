--
-- PostgreSQL database dump
--

-- Dumped from database version 11.10 (Debian 11.10-0+deb10u1)
-- Dumped by pg_dump version 11.10 (Debian 11.10-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: events; Type: TABLE; Schema: public; Owner: bduser
--

CREATE TABLE public.events (
    id integer NOT NULL,
    channel character varying(64) NOT NULL,
    nonce character varying(64) NOT NULL,
    event bytea NOT NULL
);


ALTER TABLE public.events OWNER TO bduser;

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: bduser
--

CREATE SEQUENCE public.events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO bduser;

--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bduser
--

ALTER SEQUENCE public.events_id_seq OWNED BY public.events.id;


--
-- Name: events id; Type: DEFAULT; Schema: public; Owner: bduser
--

ALTER TABLE ONLY public.events ALTER COLUMN id SET DEFAULT nextval('public.events_id_seq'::regclass);


--
-- Name: events events_nonce_key; Type: CONSTRAINT; Schema: public; Owner: bduser
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_nonce_key UNIQUE (nonce);


--
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: bduser
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--
