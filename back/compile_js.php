<?php

function do_header(){
    echo "<html>
<H1> Compilation OCaml pour Padiflac </H1>
<form action='compile_js.php' method='post' enctype='multipart/form-data'>
<input type='file'
       id='source_caml' name='source_caml'
       accept='text/ml, application/ml'>
 <input type='submit' value='Submit'>

</form>";
}

include "caml_env.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
  $target_dir = tempnam(sys_get_temp_dir(), 'ocamlcomp');
  if (file_exists($target_dir)) { unlink($target_dir); }
  mkdir($target_dir);
  $bname_info = pathinfo($_FILES["source_caml"]["name"]);
  $bname = $bname_info['filename'];
  $target_file = $target_dir ."/". $bname . ".ml";
  if(move_uploaded_file($_FILES["source_caml"]["tmp_name"], $target_file)){
   // $message=shell_exec("ocamlc tmp/fichier_caml.ml -c 2>&1");
   $message=null;
   $retvalue=null;
   exec($camlEnv. "(cd ".$target_dir."; ocamlbuild -quiet -package padiflac,js_of_ocaml,js_of_ocaml-ppx ".$bname. ".byte) 2>&1", $message,$retvalue);
   if($retvalue == 0){ 
          $message2=null;
          $retvalue2=null;
          exec($camlEnv. " (cd ".$target_dir."; js_of_ocaml ".$bname.".byte) 2>&1", $message2, $retvalue2);
          if($retvalue2 ==0 && $message2 ==null){
            header("Content-Description: File Transfer"); 
            header("Content-Type: application/javascript");
            header("Content-Disposition: attachment; filename=\"".$bname.".js\""); 
            readfile($target_dir."/".$bname.".js");
            rmdir($target_dir);
            die();

          }else {
            do_header();
            print_r(shell_exec($camlEnv . 'echo $PATH' ));
            echo "Compilling OK<br>";
            echo "fail_to translate to javascript ! <br>";
            print_r($message2);
          }
     }else{
        do_header();
         echo "fail_to compile ! <br>";
         echo "<textarea style=\"width: 50%; height: 150px;\" >";
         foreach($message as &$val) {print_r ($val); echo "\n";};
         echo "</textarea>";
     }
     }else{
        do_header();
        echo "error uploading";
     }
}else{
    do_header();
       
}

rmdir($target_dir);
?>
</html>
