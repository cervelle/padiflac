<?php

function do400() {
    http_response_code(400);
    echo "<html><body><h1>400</h1></body></html>";
}


function do500() {
    http_response_code(500);
    echo "<html><body><h1>500</h1></body></html>";
}

function listen($db,$channel) {
    $db->query("LISTEN ".$channel);
}

function notify($db,$channel) {
    $db->query("NOTIFY ".$channel);
}

function dopost($db,$channel,$nonce) {
    try {
      $data = fopen("php://input","r");
      $sql = "INSERT INTO events (channel,nonce,event) VALUES (:channel, :nonce, :data)";
      $stmt = $db->prepare($sql);
      $stmt->bindValue(1,$channel);
      $stmt->bindValue(2,$nonce);
      $stmt->bindValue(3,$data,PDO::PARAM_LOB);
      if ($stmt->execute())
          notify($db,$channel);
               
      echo("OK");
    }
    catch (PDOException $e) {
        do400();
    }
}

function getresults($db,$channel,$id) {
     $sql = "SELECT id,length(event) as l,event FROM events WHERE channel = :channel AND id >= :id";
      $stmt = $db->prepare($sql);
      $stmt->bindValue(1,$channel);
      $stmt->bindValue(2,$id);
      if (!$stmt->execute()) {
          do500();
          die($stmt->errorInfo());
      }
      return $stmt;
}


function doget($db,$channel,$id) {
    try {

      listen($db,$channel);
        
      $stmt = getresults($db,$channel,$id);

      if ($stmt->rowCount() === 0) {
          if (! $db->pgsqlGetNotify(PDO::FETCH_BOTH,5000)) {
            echo($id);
            return;
          }
          $stmt = getresults($db,$channel,$id);
      }
      header('Content-Type: application/octet-stream');

      while($o = $stmt->fetchObject()) {
          echo($o->l);
          echo("|");
          fpassthru($o->event);
          $id = $o->id+1;
      }
      echo($id);
    }
    catch (PDOException $e) {
        do400();
    }    
}

function doit() {

    require("../passwd.php");    
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
    header("Allow: POST, GET, OPTIONS");
    header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type");

    if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS')
        return;
    
    try {
        $db = new PDO('pgsql:host=localhost;dbname=bd','bduser',$passwd);
    }
    catch(PDOException $e) {
        die('ERROR: ' . $e->getMessage() . "\n");
    }

    //var_dump($_GET);
    // var_dump($db);

    if (!array_key_exists('channel',$_GET)) {
        do400();
        return;
    }
    $channel = $_GET['channel'];
    if (!preg_match("/^[a-zA-Z0-9_]{1,64}$/",$channel)) {
        do400();
        return;
    }
    switch($_SERVER['REQUEST_METHOD'])
    {
    case 'GET' :
      if (array_key_exists('id',$_GET)) {
          $id = $_GET['id'];
          if (!is_numeric($id) or $id<0) {
              do400();
              return;
          }
      } else {
          $id = 0;
      } 
      doget($db,$channel,$id);break;
    case 'POST' :
        if (!array_key_exists('nonce',$_GET)) {
            http_response_code(400);
            return;
        }
        $nonce = $_GET['nonce'];        
        if (!preg_match("/^[a-zA-Z0-9]{64}$/",$nonce)) {
            do400();
            return;
        }
        dopost($db,$channel,$nonce);break;
    }
}                                                     

doit();
?>
